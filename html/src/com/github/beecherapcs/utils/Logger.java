package com.github.beecherapcs.utils;

import com.badlogic.gdx.Gdx;

// This is a dummy Logger class that doesn't use the features that aren't supported by GWT.
public class Logger {
  public static final void log(String message) {
    Gdx.app.log("", message);
  }

  public static final void err(String message) {
    Gdx.app.error("", message);
  }

  public static final void dbg(String message) {
    Gdx.app.debug("", message);
  }
}
