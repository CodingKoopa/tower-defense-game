package com.github.beecherapcs.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import com.github.beecherapcs.TowerDefenseGame;

public class DesktopLauncher {
  public static void main(String[] arg) {
    Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
    config.setTitle("Tower Defense Game");
    //		config.setWindowedMode(1280, 720);
    config.setMaximized(true);
    new Lwjgl3Application(new TowerDefenseGame(), config);
  }
}
