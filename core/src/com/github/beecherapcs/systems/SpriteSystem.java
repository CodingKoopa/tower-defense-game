package com.github.beecherapcs.systems;

import space.earlygrey.shapedrawer.JoinType;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import com.github.beecherapcs.Mapper;
import com.github.beecherapcs.components.EnemyComponent;
import com.github.beecherapcs.components.KinematicsComponent;
import com.github.beecherapcs.components.SpriteComponent;
import com.github.beecherapcs.screens.GameScreen;
import com.github.beecherapcs.utils.CustomShapeDrawer;
import com.github.beecherapcs.utils.HpBar;
import com.github.beecherapcs.utils.Units;

public class SpriteSystem extends EntitySystem {
  // The collection of entities that this system has to operate on.
  private ImmutableArray<Entity> entities;

  private final GameScreen gameScreen;
  private final PolygonSpriteBatch batch;
  private final CustomShapeDrawer sd;

  public SpriteSystem(int priority, GameScreen gameScreen, PolygonSpriteBatch batch) {
    this.priority = priority;
    this.gameScreen = gameScreen;
    this.batch = batch;
    // TODO: Use an atlas.
    sd = new CustomShapeDrawer(batch);
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(Family.all(SpriteComponent.class, KinematicsComponent.class).get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity entity : entities) {
      Sprite sprite = Mapper.spriteMapper.get(entity).sprite;
      Vector2 pos = Mapper.kinematicsMapper.get(entity).position;
      sprite.setPosition(pos.x - sprite.getOriginX(), pos.y - sprite.getOriginY());
      sprite.draw(batch);
      Polygon p = HpBar.makeHpBar(1);
      // TODO: Add a constant for how much higher to put the bar.
      p.setPosition(pos.x, pos.y + sprite.getHeight() / 2 + 0.1f);

      EnemyComponent enemyComponent = Mapper.enemyMapper.get(entity);
      if (enemyComponent != null) {
        // TODO: Play a destruction animation.
        if (enemyComponent.hp <= 0) {
          // Let's say that money earned is a function of damage.
          gameScreen.money += enemyComponent.damage * 3;
          getEngine().removeEntity(entity);
          continue;
        }

        sd.setColor(Color.BLACK);
        sd.filledPolygon(p);
        sd.setColor(Color.RED);
        sd.filledPolygon(HpBar.cutPolygon(p, enemyComponent.hp / enemyComponent.maxHp));
        sd.setColor(Color.DARK_GRAY);
        sd.polygon(p, 1.0f / Units.pixelsPerWorldUnit, JoinType.SMOOTH);
      }
    }
  }
}
