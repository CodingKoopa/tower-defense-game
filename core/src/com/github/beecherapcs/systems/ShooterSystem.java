package com.github.beecherapcs.systems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import com.github.beecherapcs.Mapper;
import com.github.beecherapcs.ShotOrigin;
import com.github.beecherapcs.components.*;
import com.github.beecherapcs.entitytypes.RockEntityType;
import com.github.beecherapcs.screens.GameScreen;
import com.github.beecherapcs.utils.Logger;

public class ShooterSystem extends IteratingSystem {
  private final GameScreen gameScreen;

  public ShooterSystem(int priority, GameScreen gameScreen) {
    super(Family.all(TowerComponent.class).get());
    this.priority = priority;
    this.gameScreen = gameScreen;
  }

  RockEntityType entityType = new RockEntityType();

  Vector2 tmp = new Vector2();

  @Override
  public void processEntity(Entity entity, float deltaTime) {
    if (!gameScreen.isPlaying || gameScreen.isPaused) return;

    TowerComponent towerComponent = Mapper.towerMapper.get(entity);

    // TODO: Perhaps separate shotOrigins into its own component, so that this system doesn't have
    // to deal with any towers that don't pertain to it.
    if (towerComponent.shotOrigins == null) {
      Sprite sprite = Mapper.spriteMapper.get(entity).sprite;
      sprite.setRotation(
          MathUtils.radiansToDegrees * Mapper.bodyMapper.get(entity).body.getAngle());
      Mapper.bodyMapper.get(entity).body.setAngularVelocity(50);
      return;
    }

    // Accumulate time for the entity's counter, so we know when we can shoot.
    towerComponent.accumulator += deltaTime;

    float maxScore = 0;
    Array<Entity> targets = towerComponent.targets;
    if (!targets.isEmpty()) {
      Entity target = null;
      for (Entity candidateEntity : targets) {
        PathFollowComponent pathFollowComponent = Mapper.pathFollowMapper.get(candidateEntity);
        if (pathFollowComponent != null) {
          float score = pathFollowComponent.score;
          if (score > maxScore) {
            target = candidateEntity;
            maxScore = score;
          }
        }
      }
      if (target != null) {
        Sprite sprite = Mapper.spriteMapper.get(entity).sprite;

        tmp.set(Mapper.kinematicsMapper.get(entity).position);
        Vector2 p2 = Mapper.kinematicsMapper.get(target).position;
        // Calculate the angle to the target.
        float angleToTarget = tmp.sub(p2).angleDeg() + 90;
        sprite.setRotation(angleToTarget);

        // See if we're able to shoot.
        if (towerComponent.accumulator >= towerComponent.rate) {
          Logger.dbg("Shooting!");

          // Reset the accumulator.
          towerComponent.accumulator = 0;

          Vector2 towerPos = Mapper.kinematicsMapper.get(entity).position;

          for (ShotOrigin shotOrigin : towerComponent.shotOrigins) {
            // TODO: Implement pooling here.
            Entity bulletEntity = new Entity();

            Vector2 shotPos = towerPos.cpy();
            shotPos.add(shotOrigin.relPos);
            shotPos.rotateAroundDeg(towerPos, angleToTarget);

            Vector2 shotVel = shotOrigin.vel.cpy();
            shotVel.rotateDeg(angleToTarget);

            bulletEntity.add(new KinematicsComponent(shotPos, shotVel));
            bulletEntity.add(
                new DynamicsComponent(entityType.getBodyDef(), entityType.getFixtureDef()));
            bulletEntity.add(entityType.getSpriteComponent());
            bulletEntity.add(new ProjectileComponent(entityType.getDamage()));

            getEngine().addEntity(bulletEntity);
          }
        }
      }
    }
  }
}
