package com.github.beecherapcs.systems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import com.github.beecherapcs.Mapper;
import com.github.beecherapcs.components.DynamicsComponent;
import com.github.beecherapcs.components.KinematicsComponent;
import com.github.beecherapcs.components.PathFollowComponent;
import com.github.beecherapcs.screens.GameScreen;
import com.github.beecherapcs.utils.Logger;

public class PhysicsSystem extends IteratingSystem {
  private final GameScreen gameScreen;

  private final Vector2 tmp = new Vector2();

  public PhysicsSystem(int priority, GameScreen gameScreen) {
    super(Family.all(DynamicsComponent.class).get());
    this.priority = priority;
    this.gameScreen = gameScreen;
  }

  @Override
  public void processEntity(Entity entity, float deltaTime) {
    // Skipping this method shouldn't save the method, this is just done to save resources.
    if (!gameScreen.isPlaying || gameScreen.isPaused) return;

    DynamicsComponent dynamics = Mapper.dynamicsMapper.get(entity);
    KinematicsComponent kinematics = Mapper.kinematicsMapper.get(entity);
    PathFollowComponent pathFollow = Mapper.pathFollowMapper.get(entity);
    Body body = Mapper.bodyMapper.get(entity).body;

    if (dynamics.isDead) {
      getEngine().removeEntity(entity);
      return;
    }

    // If this entity is following a path, don't update its actual position and velocity.
    if (pathFollow != null) {
      // If we are in a collision or reeling from one.
      if (dynamics.activeContact || dynamics.reeling) {
        // Notify the path system of what Box2D *wants* to do.
        pathFollow.boxDesiredPosition.set(body.getPosition());
        pathFollow.boxDesiredVelocity.set(body.getLinearVelocity());

        // If we are in an active collision.
        if (dynamics.activeContact) {
          // Get the angle that the path *wants* us to travel at. Due to the fact that this system
          // runs before the path system, this is outdated by a frame (or outright wrong on the
          // first), which isn't really worth fixing. The path system forces the velocity vector
          // to match the angle of the not-outdated path velocity vector anyways.
          float theta = pathFollow.pathDesiredVelocity.angleDeg();

          Logger.dbg(
              "Path Velocity Vector: "
                  + pathFollow.pathDesiredVelocity.len()
                  + " @ "
                  + (int) theta
                  + " degrees.");
          Logger.dbg(
              "Box2D Velocity Vector: "
                  + pathFollow.boxDesiredVelocity.len()
                  + " @ "
                  + (int) pathFollow.boxDesiredVelocity.angleDeg()
                  + " degrees.");

          // Set the temporary vector to the box2d velocity vector.
          tmp.set(pathFollow.boxDesiredVelocity);
          // "Undo" the rotation.
          tmp.rotateDeg(-theta);
          // Preserve the x component of the box2d velocity vector.
          pathFollow.boxDesiredVelocity.set(tmp.x, 0);
          // Take the x component, and treat it as the whole vector.
          pathFollow.boxDesiredVelocity.rotateDeg(theta);

          Logger.dbg(
              "Box2D Velocity Vector (ADJUSTED): "
                  + pathFollow.boxDesiredVelocity.len()
                  + " @ "
                  + (int) pathFollow.boxDesiredVelocity.angleDeg()
                  + " degrees.");
        }
      }
    } else {
      kinematics.position.set(body.getPosition());
      kinematics.velocity.set(body.getLinearVelocity());
    }
  }
}
