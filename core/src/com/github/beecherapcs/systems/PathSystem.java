package com.github.beecherapcs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Body;

import com.github.beecherapcs.Mapper;
import com.github.beecherapcs.Path;
import com.github.beecherapcs.Segment;
import com.github.beecherapcs.components.DynamicsComponent;
import com.github.beecherapcs.components.KinematicsComponent;
import com.github.beecherapcs.components.PathFollowComponent;
import com.github.beecherapcs.screens.GameScreen;
import com.github.beecherapcs.utils.Logger;

public class PathSystem extends IteratingSystem {
  private final GameScreen gameScreen;
  private final Path path;

  public PathSystem(int priority, GameScreen gameScreen, Path path) {
    super(Family.all(KinematicsComponent.class, PathFollowComponent.class).get());
    this.priority = priority;
    this.gameScreen = gameScreen;
    this.path = path;
  }

  @Override
  public void processEntity(Entity entity, float deltaTime) {
    if (!gameScreen.isPlaying || gameScreen.isPaused) return;

    // Obtain the components of this entity.
    KinematicsComponent kinematics = Mapper.kinematicsMapper.get(entity);
    PathFollowComponent pathFollow = Mapper.pathFollowMapper.get(entity);
    DynamicsComponent dynamics = Mapper.dynamicsMapper.get(entity);
    Body body = Mapper.bodyMapper.get(entity).body;

    // Obtain the segment that this entity is in.
    Segment s = path.getSegment(pathFollow.segmentIdx);
    // If we just started this segment.
    if (pathFollow.t == 0)
      // Calculate the actual end time.
      pathFollow.tEnd = s.getTimeEnd(pathFollow.speed);

    // The t value on a [0.0, 1.0] scale, which is what the curves operate in.
    float normalizedT;
    // If we are in an active collision or just got knocked back, we have to account for that.
    // TODO: Consider collisions with other enemies!
    if (dynamics.activeContact || dynamics.reeling) {
      // Approximate the normalized t value for the desired position, on the curve.
      normalizedT = s.spline.locate(pathFollow.boxDesiredPosition);

      // Work backwards for our actual t value, since that's what we operate in.
      pathFollow.t = normalizedT * pathFollow.tEnd;

      // If the normalized t value is less than the domain, we've been knocked back a segment.
      while (normalizedT <= 0 && pathFollow.segmentIdx != 0) {
        --pathFollow.segmentIdx;
        Logger.dbg("Knocked back into segment " + pathFollow.segmentIdx + ".");
        // Reset our t value so that, next time this entity is processed, the end time will be
        // recalculated.
        pathFollow.t = 0;
        s = path.getSegment(pathFollow.segmentIdx);
        normalizedT = s.spline.locate(pathFollow.boxDesiredPosition);
      }
    } else {
      pathFollow.t += Gdx.graphics.getDeltaTime() * pathFollow.speed;
      normalizedT = pathFollow.t / pathFollow.tEnd;
    }

    // If the normalized T value is greater than the domain,
    if (normalizedT >= 1) {
      if (pathFollow.segmentIdx == path.getNumSegments() - 1) {
        Logger.log("Finished following path, removing entity.");
        gameScreen.health -= Mapper.enemyMapper.get(entity).damage;
        getEngine().removeEntity(entity);
        // Don't remove all of the components because we still need the body component in order to
        // properly delete the body from the Box2D world.
        return;
      } else {
        s = path.getSegment(++pathFollow.segmentIdx);
        Logger.log("Proceeding into segment " + pathFollow.segmentIdx + ".");
        // Since we are at the beginning of the next segment, t should be approximately 0.
        pathFollow.t = 0;

        // With this approximation we made, 0 is 0 - no normalization is needed.
        normalizedT = pathFollow.t;
      }
    }

    // No matter what, we want the object to be at the normalized T value now.
    s.spline.valueAt(kinematics.position, normalizedT);
    // Write the position on the path back to Box2D.
    body.setTransform(kinematics.position, 0);

    // This is the velocity that the path currently *wants* the object to be moving at.
    s.spline.derivativeAt(pathFollow.pathDesiredVelocity, normalizedT);
    // Convert the velocity away from the [0, 1] scale.
    pathFollow.pathDesiredVelocity.scl(1 / pathFollow.tEnd);
    // If we have the extra factor of Box2D contact or reeling from contact, we need to fix
    // the velocity accordingly.
    if (dynamics.activeContact || dynamics.reeling) {
      // Apply the velocity in the opposite direction as the path. This is actually mostly
      // unnecessary, because the physics system should have already adjusted the velocity, but it
      // didn't have the most up to date path velocity.
      pathFollow.boxDesiredVelocity.setAngleDeg(pathFollow.pathDesiredVelocity.angleDeg() + 180);

      kinematics.velocity.set(pathFollow.boxDesiredVelocity);
    } else {
      kinematics.velocity.set(pathFollow.pathDesiredVelocity);
    }
    // Write the velocity on the path back to Box2D.
    body.setLinearVelocity(kinematics.velocity);

    // TODO: Is it okay that this check is so late?
    // If we are reeling (or are in a very slow collision), see if we're done yet.
    // TODO add back activecontact
    if (dynamics.activeContact || dynamics.reeling) {
      float velocityMagnitude = kinematics.velocity.len();
      // If we're done reeling.
      if (velocityMagnitude <= 0.5) {
        Logger.dbg("Done reeling @ " + velocityMagnitude);
        dynamics.activeContact = false;
        dynamics.reeling = false;
      } else {
        Logger.dbg("Reeling @ " + velocityMagnitude);
      }
    }

    // Update the score, so that we can target the entity that is the furthest ahead.
    pathFollow.score = pathFollow.segmentIdx + normalizedT;
  }
}
