package com.github.beecherapcs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import com.github.beecherapcs.utils.CustomShapeDrawer;
import com.github.beecherapcs.utils.Logger;
import com.github.beecherapcs.utils.Units;

// TODO: Verify that one segment picks up where the last ended.
// TODO: Draw as a path, rather than line sequence.
public class Path {
  private final Segment[] segments;

  private final CustomShapeDrawer sd;

  private final float lineWidth = 0.05f;

  public Path(Batch batch, Segment[] segments) {
    this.sd = new CustomShapeDrawer(batch);
    this.segments = segments;
  }

  public Path(Batch batch, FileHandle file) {
    this(batch, file, -1);
  }

  public Path(Batch batch, FileHandle file, float worldHeight) {
    this.sd = new CustomShapeDrawer(batch);
    this.segments = loadFromFile(file, worldHeight);
  }

  private enum Command {
    InvalidCommand,
    StartCurve,
    ContinueCurve
  }

  private static Segment[] loadFromFile(FileHandle file, float worldHeight) {
    BufferedReader reader = new BufferedReader(new StringReader(file.readString()));

    Array<Segment> segments = new Array<>(true, 30, Segment.class);

    String line = null;
    Vector2 lastPoint = null;
    boolean convertToYDown = false;

    try {
      while ((line = reader.readLine()) != null) {
        if (!line.isEmpty() && line.charAt(0) != '#') {
          String[] commandLine = line.split("\\s+");

          int commandLineIdx = 0;

          Command command = Command.InvalidCommand;
          char commandChar = commandLine[commandLineIdx++].charAt(0);
          if (commandChar == 'y') {
            if (commandLine.length < 2) {
              Logger.err("Error: Missing argument to y, ignoring.");
              continue;
            }
            String arg = commandLine[commandLineIdx++];
            if (arg.equals("down")) {
              if (worldHeight == -1) {
                Logger.err("Error: y-down specified, but world height has not been set, ignoring.");
                continue;
              }
              Logger.dbg("Using y-down system.");
              convertToYDown = true;
            } else if (arg.equals("up")) {
              Logger.dbg("Using y-up system.");
              convertToYDown = false;
            } else {
              Logger.err("Error: Unknown argument \"" + arg + "\" to y, ignoring.");
            }
          } else if (commandChar == 's') {
            Logger.dbg("Starting new curve.");
            command = Command.StartCurve;
          } else if (commandChar == 'c') {
            Logger.dbg("Continuing curve.");
            command = Command.ContinueCurve;
            if (lastPoint == null) {
              Logger.err(
                  "Error: No last point to use for continueCurve, ignoring. Make sure you began with startCurve.");
              continue;
            }
          } else {
            Logger.err("Error: unknown command " + commandLine[0] + ", ignoring.");
            continue;
          }

          if (command == Command.StartCurve || command == Command.ContinueCurve) {
            int numArguments = commandLine.length - 1;

            if (numArguments % 2 != 0) {
              Logger.err(
                  "Error: Number of arguments isn't even, ignoring. Make sure you specify the points in pairs.");
              continue;
            }

            int numPoints = numArguments / 2;
            if (command == Command.ContinueCurve) numPoints += 1;
            Vector2[] points = new Vector2[numPoints];
            int pointIdx = 0;
            if (command == Command.ContinueCurve) points[pointIdx++] = lastPoint;

            while (commandLineIdx < commandLine.length) {
              points[pointIdx] =
                  new Vector2(
                      (float) Integer.parseInt(commandLine[commandLineIdx++]),
                      (float) Integer.parseInt(commandLine[commandLineIdx++]));
              points[pointIdx].scl(1.0f / Units.pixelsPerWorldUnit);
              if (convertToYDown) points[pointIdx].y = worldHeight - points[pointIdx].y;
              ++pointIdx;
            }
            segments.add(new Segment(points));
            lastPoint = points[points.length - 1];
          }
        }
      }
    } catch (IOException e) {
      Logger.err("Error: Failed to load path: " + e);
    }
    segments.shrink();
    return segments.items;
  }

  public Segment getSegment(int index) {
    return segments[index];
  }

  public int getNumSegments() {
    return segments.length;
  }

  // TODO: note about projection matrix
  // TODO: draw circles for control points! that would be cool
  // TODO: call sd.update() when appropriate!
  public void debugDraw() {
    for (Segment s : segments) {
      // Go through the path points.
      for (int i = 0; i < s.pathPoints.length - 1; ++i)
        sd.line(s.pathPoints[i], s.pathPoints[i + 1], Color.RED, lineWidth);

      for (int i = 0; i < s.splinePoints.length - 1; ++i)
        sd.line(s.splinePoints[i], s.splinePoints[i + 1], Color.PINK, lineWidth);
    }
  }

  public Vector2 getInitialPosition() {
    return segments[0].pathPoints[0];
  }
}
