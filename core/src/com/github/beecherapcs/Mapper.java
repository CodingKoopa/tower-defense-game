package com.github.beecherapcs;

import com.badlogic.ashley.core.ComponentMapper;

import com.github.beecherapcs.components.*;

public class Mapper {
  // TODO: Investigate potential issues with using static on Android.
  // TODO: Order
  public static final ComponentMapper<SpriteComponent> spriteMapper =
      ComponentMapper.getFor(SpriteComponent.class);
  public static final ComponentMapper<KinematicsComponent> kinematicsMapper =
      ComponentMapper.getFor(KinematicsComponent.class);
  public static final ComponentMapper<DynamicsComponent> dynamicsMapper =
      ComponentMapper.getFor(DynamicsComponent.class);
  public static final ComponentMapper<BodyComponent> bodyMapper =
      ComponentMapper.getFor(BodyComponent.class);
  public static final ComponentMapper<PathFollowComponent> pathFollowMapper =
      ComponentMapper.getFor(PathFollowComponent.class);
  public static final ComponentMapper<TowerComponent> towerMapper =
      ComponentMapper.getFor(TowerComponent.class);
  public static final ComponentMapper<EnemyComponent> enemyMapper =
      ComponentMapper.getFor(EnemyComponent.class);
  public static final ComponentMapper<ProjectileComponent> projectileMapper =
      ComponentMapper.getFor(ProjectileComponent.class);
}
