package com.github.beecherapcs.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import com.github.beecherapcs.TowerDefenseGame;
import com.github.beecherapcs.utils.Logger;

public class MainMenuScreen implements Screen {
  private final TowerDefenseGame game;

  private final OrthographicCamera camera;
  private final Viewport viewport;

  private final Texture backgroundTexture;

  private float centerX;
  private float centerY;

  private long startZoomTime = -1;

  public MainMenuScreen(TowerDefenseGame game) {
    Logger.log("Initializing main menu screen.");

    // Initialize our reference to the game object.
    this.game = game;

    // Load the background image.
    backgroundTexture = new Texture(Gdx.files.internal("textures/MainMenuBackground.jpg"));

    // Initialize the camera. This determines how the world will be viewed.
    camera = new OrthographicCamera();

    // Initialize the viewport. This manages the camera's viewport width and height. Use a
    // ScreenViewport so that
    // no scaling occurs. We can do this here without any repercussions because it won't affect game
    // logic.
    viewport = new ScreenViewport(camera);
  }

  @Override
  public void render(float delta) {
    // Clear the screen.
    ScreenUtils.clear(0.2f, 0, 0.2f, 1);

    // Update the camera matrices.
    camera.update();

    // Use the updated camera coordinate system for the sprite batch.
    game.batch.setProjectionMatrix(camera.combined);
    // Start recording drawing commands for OpenGL.
    game.batch.begin();
    // Tint the images with black.
    game.batch.setColor(1, 1, 1, 0.25f);
    // Draw the background.
    game.batch.draw(backgroundTexture, 0, 0, viewport.getWorldWidth(), viewport.getWorldHeight());
    // Disable tinting.
    game.batch.setColor(Color.WHITE);
    // Draw the title text.
    game.font.draw(game.batch, "Tower Defense Game", centerX - 70, centerY);
    // Draw the start prompt.
    game.font.draw(game.batch, "Click anywhere to begin!", centerX - 75, centerY - 50);
    // Stop recording drawing requests.
    game.batch.end();

    if (startZoomTime == -1) {
      // If the screen is touched/clicked.
      // TODO: if debug
      if (Gdx.input.isTouched()) {
        startZoomTime = TimeUtils.nanoTime();
      }
    } else if (TimeUtils.nanoTime() - startZoomTime < 1000000000) {
      // TODO: pre-calculate bezier curves here?
      camera.zoom -= 1 * Gdx.graphics.getDeltaTime();
    } else {
      // Set the screen to a new game screen.
      game.setScreen(new GameScreen(game));
      // Dispose of this screen.
      dispose();
    }
  }

  @Override
  public void resize(int width, int height) {
    // Update the viewport width and height.
    viewport.update(width, height, true);
    // (Hopefully temporary) lazy solution for us to draw centered text.
    centerX = viewport.getWorldWidth() / 2;
    centerY = viewport.getWorldHeight() / 2;
  }

  @Override
  public void show() {}

  @Override
  public void hide() {}

  @Override
  public void pause() {}

  @Override
  public void resume() {}

  @Override
  public void dispose() {}
}
