package com.github.beecherapcs.screens;

import com.badlogic.ashley.core.*;
import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import com.github.beecherapcs.*;
import com.github.beecherapcs.Path;
import com.github.beecherapcs.components.DynamicsComponent;
import com.github.beecherapcs.components.EnemyComponent;
import com.github.beecherapcs.listeners.CustomContactListener;
import com.github.beecherapcs.listeners.DynamicEntityListener;
import com.github.beecherapcs.systems.PathSystem;
import com.github.beecherapcs.systems.PhysicsSystem;
import com.github.beecherapcs.systems.ShooterSystem;
import com.github.beecherapcs.systems.SpriteSystem;
import com.github.beecherapcs.utils.LeftFitViewport;
import com.github.beecherapcs.utils.Logger;
import com.github.beecherapcs.utils.Units;

// import com.gmail.blueboxware.libgdxplugin.annotations.GDXAssets;

public class GameScreen implements Screen {
  // Reference to the game object.
  private final TowerDefenseGame game;

  // The preferences for debug mode.
  // TODO: Make a partially dummy Preferences object that always returns false if this isn't a debug
  // build.
  private final Preferences debugPreferences;
  private final String showDebugKey = "showDebug";
  private final String gameUiDebugKey = "gameUiDebug";
  private final String debugUiDebugKey = "debugUiDebug";
  private final String pathDebugKey = "pathDebug";
  private final String boxDebugKey = "boxDebug";

  // The texture for the background image.
  private final Texture backgroundTexture;
  // The sprite for the background image, including the size it will be drawn at..
  private final Sprite backgroundSprite;

  // TODO: Tune this value.
  private final float menuWidth = Units.pixelsToWorldUnits(150);

  // The camera. This determines how the world will be viewed.
  private final OrthographicCamera camera;
  // The viewport. This manages the camera's viewport width and height. Use a FitViewport so that
  // gameplay is restricted to the map area, and there are the least surprises.
  private final Viewport viewport;

  //  @GDXAssets(skinFiles = {"android/assets/skin/uiskin.json"})
  private final Skin skin;

  // The stage for the game UI.
  private final Stage gameStage;
  private final Table rootTable;
  private final Table overlayTable;
  private final Label livesLabel;
  private final Label moneyLabel;
  private final Label roundLabel;
  private final Window sidebarWindow;
  private final TextButton startRoundButton;
  private final String startText = "Start Round";
  private final Window congratsWindow;

  // The stage for the debug UI.
  private final Stage debugStage;
  private final Window debugWindow;

  // The Box2D world.
  private final World world;
  // The amount of time "produced" by the renderer that needs to be stepped through by the physics
  // engine.
  private float physicsAccumulator = 0;

  // The path containing the segments for the enemies to follow.
  private final Path path;

  // The entity engine.
  private final Engine engine;

  // The family of enemy entities, used to determine how many are left.
  private final Family enemyFamily;

  // The shared sprite batch.
  private final PolygonSpriteBatch batch = new PolygonSpriteBatch();

  // The debug renderers that draw on top of everything else.
  private final Box2DDebugRenderer debugBox2DRenderer = new Box2DDebugRenderer();

  // The tower spawner that adds towers and creates the tower menu UI.
  private final TowerSpawner towerSpawner;
  // The enemy spawner that reads the schedule and spawns enemys when necessary.
  private final EnemySpawner enemySpawner;

  // TODO: These should *probably* be accessed via getters/setters.
  public boolean isPlaying = false;
  public boolean isPaused = false;
  public int health = 200;
  public int money = 200;
  public int round = 0;

  public GameScreen(TowerDefenseGame game) {
    // TODO: Setup debug mode that toggles debug messages.
    // Initialize our reference to the game object.
    this.game = game;

    Logger.log("Loading preferences.");

    debugPreferences = Gdx.app.getPreferences("Debug");

    Logger.log("Loading assets.");

    backgroundTexture = new Texture(Gdx.files.internal("textures/Map.png"));

    final float worldWidth = (float) backgroundTexture.getWidth() / Units.pixelsPerWorldUnit;
    final float worldHeight = (float) backgroundTexture.getHeight() / Units.pixelsPerWorldUnit;

    backgroundSprite = new Sprite(backgroundTexture);
    backgroundSprite.setSize(worldWidth, worldHeight);

    Logger.log("Initializing world view.");

    camera = new OrthographicCamera();
    // TODO: This doesn't make enough room for the menu *with padding*, therefore things don't work
    // very well at small resolutions.
    viewport =
        new LeftFitViewport(worldWidth, worldHeight, camera, Units.worldUnitsToPixels(menuWidth));

    Logger.log("Initializing physics engine.");

    // Initialize Box2D.
    Box2D.init();

    // Initialize the world. We are disabling gravity since this is a top-down game.
    world = new World(new Vector2(0, 0), true);

    Logger.log("Initializing path.");

    // Initialize the path.
    path = new Path(batch, Gdx.files.internal("paths/Test.path"), worldHeight);

    Logger.log("Initializing entity engine.");

    // Initialize the Ashley engine.
    engine = new Engine();

    // TODO: Move back up to physics engine section?
    // Register the contact listener so that we can handle collisions with the enemies.
    world.setContactListener(new CustomContactListener(engine));

    // Initialize the entity family.
    enemyFamily = Family.all(EnemyComponent.class).get();

    // Initialize the physics listener.
    engine.addEntityListener(
        Family.all(DynamicsComponent.class).get(), new DynamicEntityListener(world));

    int priority = 0;
    // Calculate Box2D physics first.
    engine.addSystem(new PhysicsSystem(priority++, this));
    // Calculate path movements, which depend on what Box2D does.
    engine.addSystem(new PathSystem(priority++, this, path));
    // Process tower aiming now that the enemy positions are good.
    engine.addSystem(new ShooterSystem(priority++, this));
    // Draw all of the sprites.
    engine.addSystem(new SpriteSystem(priority++, this, batch));

    Logger.log("Initializing skin.");
    skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
    // TODO: Set this in the JSON
    skin.getFont("default-font").setUseIntegerPositions(false);
    skin.getFont("pretty-large-font").setUseIntegerPositions(false);
    skin.getFont("large-font").setUseIntegerPositions(false);

    Logger.log("Initializing tower spawner.");
    towerSpawner = new TowerSpawner(engine, this, viewport, skin);

    Logger.log("Initializing enemy spawner.");
    enemySpawner = new EnemySpawner(engine, this, Gdx.files.internal("schedules/Test.sched"));

    Logger.log("Initializing game stage.");

    gameStage = new Stage(new ScreenViewport());
    gameStage.setDebugAll(debugPreferences.getBoolean(gameUiDebugKey));

    rootTable = new Table(skin);
    rootTable.setFillParent(true);
    gameStage.addActor(rootTable);

    overlayTable = new Table(skin);
    // The width will be set dynamically during the resize event.
    rootTable.add(overlayTable).left().top();

    Table topLeftTable = new Table(skin);
    livesLabel = new Label("", skin, "header");
    moneyLabel = new Label("", skin, "header");
    topLeftTable.add(livesLabel).pad(20);
    topLeftTable.add(moneyLabel).pad(20);
    overlayTable.add(topLeftTable);
    overlayTable.add().expandX();
    roundLabel = new Label("", skin, "header");
    overlayTable.add(roundLabel).right().pad(20);
    overlayTable.row();
    overlayTable.add().expandY();
    overlayTable.row();
    overlayTable.add("");
    overlayTable.add().expandX();
    overlayTable.add("");

    sidebarWindow = new Window("", skin);
    sidebarWindow.setMovable(false);
    // TODO: Make these into constants.
    // TODO: Make larger only on Android?
    sidebarWindow.pad(40);
    sidebarWindow.padTop(100);
    // Populate the window with the towers.
    towerSpawner.initTowers(sidebarWindow);
    sidebarWindow.row();
    sidebarWindow.add().expandY();
    sidebarWindow.row();
    final String play = "Play";
    final String pause = "Pause";
    startRoundButton = new TextButton(startText, skin, "header");
    // Store this in a variable so that we can execute it with a hotkey.
    final ClickListener clickListener =
        new ClickListener() {
          public void clicked(InputEvent event, float x, float y) {
            // uwu
            if (!isPlaying) isPlaying = true;
            else isPaused = !isPaused;
            startRoundButton.setText(isPaused ? play : pause);
          }
        };
    startRoundButton.addListener(clickListener);
    sidebarWindow.add(startRoundButton).colspan(2);
    rootTable.add(sidebarWindow).expand().top();

    congratsWindow = new Window("Congratulations", skin);
    congratsWindow.setVisible(false);
    congratsWindow.setResizable(false);
    congratsWindow.add("You've beaten the level!");
    congratsWindow.pack();
    gameStage.addActor(congratsWindow);

    gameStage.addListener(
        new InputListener() {
          @Override
          public boolean keyDown(InputEvent event, int keycode) {
            if (keycode == Input.Keys.ESCAPE) {
              // If we're already not playing, don't do anything.
              if (isPlaying) clickListener.clicked(null, 0, 0);
            }
            return false;
          }
        });

    // TODO: If debug.
    Logger.log("Initializing debug stage.");

    debugStage = new Stage(new ScreenViewport());
    debugStage.setDebugAll(debugPreferences.getBoolean(debugUiDebugKey));

    debugWindow = new Window("The Bug Menu", skin, "resizable");
    debugWindow.setResizable(true);
    debugStage.addActor(debugWindow);

    final Button showDebugMenuButton =
        addDebugButton(debugWindow, "Show debug menu (F7)", showDebugKey, 2);
    debugWindow.row();
    addDebugButton(debugWindow, "Draw game UI debug", gameUiDebugKey, 1, gameStage);
    addDebugButton(debugWindow, "Draw debug UI debug", debugUiDebugKey, 1, debugStage);
    debugWindow.row();
    addDebugButton(debugWindow, "Draw path debug", pathDebugKey);
    addDebugButton(debugWindow, "Draw Box2D debug", boxDebugKey);
    // This is necessary to make the window contents take up as much space as they ought to.
    debugWindow.pack();

    debugStage.addListener(
        new InputListener() {
          @Override
          public boolean keyDown(InputEvent event, int keycode) {
            if (keycode == Input.Keys.F7) {
              boolean checked = !debugPreferences.getBoolean(showDebugKey);
              debugPreferences.putBoolean(showDebugKey, checked);
              debugPreferences.flush();
              debugWindow.setTouchable(checked ? Touchable.enabled : Touchable.disabled);
              showDebugMenuButton.setChecked(checked);
            }
            return false;
          }
        });

    Logger.log("Initializing input.");

    InputMultiplexer multiplexer = new InputMultiplexer(gameStage);
    // TODO: If debug build.
    multiplexer.addProcessor(debugStage);
    Gdx.input.setInputProcessor(multiplexer);
  }

  private Button addDebugButton(Table table, final String text, final String key) {
    return addDebugButton(table, text, key, 1, null);
  }

  private Button addDebugButton(Table table, final String text, final String key, int colSpan) {
    return addDebugButton(table, text, key, colSpan, null);
  }

  private Button addDebugButton(
      Table table, final String text, final String key, int colSpan, final Stage subjectStage) {
    final CheckBox button = new CheckBox(text, skin);
    button.setChecked(debugPreferences.getBoolean(key));
    button.addListener(
        new ClickListener() {
          public void clicked(InputEvent event, float x, float y) {
            boolean checked = button.isChecked();
            debugPreferences.putBoolean(key, checked);
            debugPreferences.flush();

            // Handle some special cases.

            // If we are showing/hiding the debug menu, update whether it can be interacted with.
            if (key.equals(showDebugKey))
              debugWindow.setTouchable(checked ? Touchable.enabled : Touchable.disabled);

            if (subjectStage != null) subjectStage.setDebugAll(checked);
          }
        });
    button.getImageCell().padRight(10);
    table.add(button).colspan(colSpan);
    return button;
  }

  @Override
  public void resize(int width, int height) {
    viewport.update(width, height, true);
    gameStage.getViewport().update(width, height, true);

    // Update the table sizes ourselves so we have more control.
    rootTable.getCell(overlayTable).size(viewport.getScreenWidth(), height);
    rootTable.getCell(sidebarWindow).size(width - viewport.getScreenWidth(), height);

    debugWindow.setPosition(
        width / 2f - debugWindow.getWidth() / 2f, height / 2f - debugWindow.getHeight() / 2f);
    congratsWindow.setPosition(
        width / 2f - congratsWindow.getWidth() / 2f, height / 2f - congratsWindow.getHeight() / 2f);
  }

  @Override
  public void render(float deltaTime) {
    // Handle Input

    gameStage.act(deltaTime);

    // Debug Camera
    // TODO: If debug build.
    if (Gdx.input.isKeyPressed(Input.Keys.A)) {
      camera.zoom += 1.0 / Units.pixelsPerWorldUnit;
    }
    if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
      camera.zoom -= 1.0 / Units.pixelsPerWorldUnit;
    }
    if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
      camera.translate(-0.25f, 0, 0);
    }
    if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
      camera.translate(0.25f, 0, 0);
    }
    if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
      camera.translate(0, -0.25f, 0);
    }
    if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
      camera.translate(0, 0.25f, 0);
    }
    if (Gdx.input.isKeyPressed(Input.Keys.W)) {
      camera.rotate(-1, 0, 0, 1);
    }
    if (Gdx.input.isKeyPressed(Input.Keys.E)) {
      camera.rotate(1, 0, 0, 1);
    }

    // Draw Layer: Background

    // Clear the screen.
    // TODO: Maybe we should think more about this color choice ;)
    ScreenUtils.clear(0, 0, 0.2f, 1);

    // Draw Layer: Game Viewport

    // Check for if we're playing and unpaused, For the entity engine, the systems themselves
    // implement this check
    if (isPlaying && !isPaused) {
      // Spawn any new enemies that are scheduled for this time.
      enemySpawner.spawnEnemies(deltaTime);
      // Update the physics of the world.
      updatePhysics(deltaTime);

      // Check if we are done with this round.
      // First, check if we are done spawning enemies.
      if (enemySpawner.isDoneSpawning()) {
        // Then, check if there are any spawned enemy entities left.
        if (engine.getEntitiesFor(enemyFamily).size() == 0) {
          Logger.dbg("Done with round!");
          isPlaying = false;

          ++round;

          if (enemySpawner.isDoneWithRounds()) {
            startRoundButton.setVisible(false);
            startRoundButton.setTouchable(Touchable.disabled);
            congratsWindow.setVisible(true);
          } else {
            startRoundButton.setText(startText);
          }
        }
      }
    }

    // We have more than one viewport, so we have to apply them before drawing to each.
    viewport.apply();
    // Start recording drawing commands.
    batch.begin();
    // Update the projection matrix.
    batch.setProjectionMatrix(camera.combined);
    // Draw the background.
    backgroundSprite.draw(batch);
    // Update the entities of the world, including drawing them.
    engine.update(deltaTime);
    // Draw the path if we need to.
    if (debugPreferences.getBoolean(pathDebugKey)) path.debugDraw();
    // Stop drawing.
    batch.end();

    if (debugPreferences.getBoolean(boxDebugKey))
      // The debug Box2D renderer doesn't use the batch.
      debugBox2DRenderer.render(world, camera.combined);

    // Draw Layer: Game UI

    livesLabel.setText("Lives: " + Integer.toString(health));
    moneyLabel.setText("Money: " + Integer.toString(money));
    roundLabel.setText("Round " + Integer.toString(round + 1));

    // TODO: Address raw use of parameterized class, and the iterator issue .
    for (Cell sidebarIter : sidebarWindow.getCells().iterator()) {
      // This is the table for this particular tower.
      Table towerTable = (Table) sidebarIter.getActor();
      // These null checks pick out the window title bar, and the round start button.
      if (towerTable != null) {
        Object userObject = towerTable.getUserObject();
        if (userObject != null) {
          int cost = (int) userObject;
          for (Cell cellIter : towerTable.getCells().iterator()) {
            Actor actor = cellIter.getActor();
            if (actor instanceof Image) {
              Image image = (Image) actor;
              if (cost > money) image.setColor(Color.GRAY);
              else image.setColor(Color.WHITE);
            } else if (actor instanceof Label) {
              Label label = (Label) actor;
              if (cost > money) label.setColor(Color.RED);
              else label.setColor(Color.WHITE);
            }
          }
        }
      }
    }

    gameStage.getViewport().apply();
    gameStage.draw();

    // Draw Layer: Debug UI

    if (debugPreferences.getBoolean(showDebugKey)) debugStage.draw();
  }

  private void updatePhysics(float deltaTime) {
    // This is the upper bound we put on the frame time. We can't guarantee that the physics
    // simulation will
    // function properly at *every* dt, so we have to restrict its domain.
    final float maximumFrameTime = 0.25f;
    // This is the fixed time delta that we force the physics simulation to run at.
    final float timeStep = 1 / 60f;
    // Number of passes to make during the velocity phase of the constraint solver.
    final int velocityIterations = 6;
    // Number of passes to make during the position phase.
    final int positionIterations = 2;

    // Restrict the domain of dt.
    float frameTime = Math.min(deltaTime, maximumFrameTime);
    // Add the time that has been "produced" by the renderer to the accumulator.
    physicsAccumulator += frameTime;
    // While there are time steps to be consumed.
    while (physicsAccumulator >= timeStep) {
      // Process the time step.
      world.step(timeStep, velocityIterations, positionIterations);
      // Consume this time step.
      physicsAccumulator -= timeStep;
    }
  }

  @Override
  public void show() {}

  @Override
  public void hide() {}

  @Override
  public void pause() {}

  @Override
  public void resume() {}

  @Override
  public void dispose() {
    batch.dispose();
    gameStage.dispose();
    backgroundTexture.dispose();
    debugBox2DRenderer.dispose();
  }
}
