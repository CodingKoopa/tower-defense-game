package com.github.beecherapcs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import com.github.beecherapcs.components.DynamicsComponent;
import com.github.beecherapcs.components.EnemyComponent;
import com.github.beecherapcs.components.KinematicsComponent;
import com.github.beecherapcs.components.PathFollowComponent;
import com.github.beecherapcs.entitytypes.DummyEnemyEntityType;
import com.github.beecherapcs.entitytypes.EnemyEntityType;
import com.github.beecherapcs.screens.GameScreen;
import com.github.beecherapcs.utils.Logger;

public class EnemySpawner {
  private final Engine engine;
  private final GameScreen gameScreen;

  float t = 0;

  private final ObjectMap<String, EnemyEntityType> enemyTypes = new ObjectMap<>();

  private final Array<Array<Spawn>> spawnSchedule;

  private boolean isDoneSpawning;

  // TODO: Make more things final, like here.
  public EnemySpawner(Engine engine, final GameScreen gameScreen) {
    this.engine = engine;
    this.gameScreen = gameScreen;
    this.spawnSchedule = new Array<>(true, 30);

    initEnemyTypes();

    this.spawnSchedule.sort();
  }

  public EnemySpawner(Engine engine, final GameScreen gameScreen, FileHandle file) {
    this.engine = engine;
    this.gameScreen = gameScreen;

    initEnemyTypes();

    this.spawnSchedule = loadFromFile(file);

    for (Array<Spawn> schedule : this.spawnSchedule) schedule.sort();
  }

  public void initEnemyTypes() {
    enemyTypes.put("dummy", new DummyEnemyEntityType());
  }

  private Array<Array<Spawn>> loadFromFile(FileHandle file) {
    BufferedReader reader = new BufferedReader(new StringReader(file.readString()));

    Array<Array<Spawn>> schedule = new Array<>(true, 30, Array.class);
    Array<Spawn> roundSchedule = null;
    float t = 0;

    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
        if (!line.isEmpty() && line.charAt(0) != '#') {
          String[] commandLine = line.split("\\s+");

          // Start a new round.
          if (commandLine[0].charAt(0) == 'r') {
            roundSchedule = new Array<>(true, 30, Spawn.class);
            schedule.add(roundSchedule);
          } else {
            if (commandLine.length != 2) {
              Logger.err("Error: Wrong number of arguments, ignoring.");
              continue;
            }

            if (roundSchedule == null) {
              Logger.err("Error: Round command hasn't been ran, ignoring.");
              continue;
            }

            t += Float.parseFloat(commandLine[0]);

            EnemyEntityType enemyType = enemyTypes.get(commandLine[1]);
            if (enemyType == null) {
              Logger.err("Enemy type " + commandLine[1] + " isn't valid, using dummy.");
              enemyType = new DummyEnemyEntityType();
            }

            roundSchedule.add(new Spawn(t, enemyType));
          }
        }
      }
    } catch (IOException e) {
      Logger.err("Error: Failed to load spawn schedule: " + e);
    }
    return schedule;
  }

  public void spawnEnemies(float deltaTime) {
    t += deltaTime;
    Array<Spawn> schedule = spawnSchedule.get(gameScreen.round);
    for (Iterator<Spawn> iter = schedule.iterator(); iter.hasNext(); ) {
      Spawn spawn = iter.next();
      if (spawn.t <= t) {
        // TODO: Implement pooling of these entities?
        Entity enemyEntity = new Entity();

        // TODO: test without cpy
        enemyEntity.add(new KinematicsComponent());
        enemyEntity.add(
            new DynamicsComponent(spawn.enemyType.getBodyDef(), spawn.enemyType.getFixtureDef()));
        enemyEntity.add(
            new EnemyComponent(spawn.enemyType.getMaxHp(), spawn.enemyType.getDamage()));
        enemyEntity.add(new PathFollowComponent(spawn.enemyType.getSpeed()));
        enemyEntity.add(spawn.enemyType.getSpriteComponent());
        // The body component will be added by the entity listener.

        engine.addEntity(enemyEntity);

        iter.remove();
      } else {
        // Since the spawn schedule is ordered, if this enemy can't be spawned yet, then we may
        // assume that the rest after it can't be spawned either.
        break;
      }
    }
    isDoneSpawning = schedule.isEmpty();
  }

  public boolean isDoneSpawning() {
    return isDoneSpawning;
  }

  public boolean isDoneWithRounds() {
    return gameScreen.round >= spawnSchedule.size;
  }
}
