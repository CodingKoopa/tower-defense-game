package com.github.beecherapcs;

import com.badlogic.gdx.math.Vector2;

import com.github.beecherapcs.utils.CustomBezier;
import com.github.beecherapcs.utils.Logger;

public class Segment {
  public final Vector2[] pathPoints;
  public CustomBezier spline;
  // TODO: only if debug
  // The number of points to use for the precalculated spline.
  private final int numSplinePoints = 100;
  // The points of the spline calculated from the path. This is only used for debugging purposes,
  // the actual points
  // for the sprites to use are calculated on the fly.
  public final Vector2[] splinePoints = new Vector2[numSplinePoints];
  public float distance;

  public Segment(Vector2... points) {
    pathPoints = points;

    spline = new CustomBezier(pathPoints);
    distance = spline.approxLength(10);
    Logger.log("Initialized path segment with distance " + distance + ".");

    // TODO: If debug
    for (int i = 0; i < numSplinePoints; ++i) {
      splinePoints[i] = new Vector2();
      // Subtract 1 from the number of spline points so that this approaches 1/1.
      // TODO: This breaks when there are only two points (spanCount = -1).
      spline.valueAt(splinePoints[i], ((float) i / (numSplinePoints - 1)));
    }
  }

  // Gets the time at which the full distance of this segment will be traveled at the given speed.
  public float getTimeEnd(float speed) {
    return distance / speed;
  }
}
