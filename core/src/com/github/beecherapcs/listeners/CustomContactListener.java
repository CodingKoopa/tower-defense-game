package com.github.beecherapcs.listeners;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.*;

import com.github.beecherapcs.Mapper;
import com.github.beecherapcs.components.*;
import com.github.beecherapcs.utils.Logger;

// This class handles Box2D collisions.
public class CustomContactListener implements ContactListener {
  private final Engine engine;

  public CustomContactListener(Engine engine) {
    this.engine = engine;
  }

  @Override
  public void beginContact(Contact contact) {
    Logger.dbg("Contact started.");

    Entity entityA = (Entity) contact.getFixtureA().getUserData();
    Entity entityB = (Entity) contact.getFixtureB().getUserData();

    TowerComponent towerComponentA = Mapper.towerMapper.get(entityA);
    if (towerComponentA != null) {
      // TODO: This is a hack!
      if (towerComponentA.shotOrigins != null) {
        towerComponentA.targets.add(entityB);
      } else {
        EnemyComponent enemyComponentB = Mapper.enemyMapper.get(entityB);
        if (enemyComponentB != null) {
          // TODO: Don't hardcode...
          enemyComponentB.hp -= 1f;
        }
      }
      return;
    } else {
      TowerComponent towerComponentB = Mapper.towerMapper.get(entityB);
      if (towerComponentB != null) {
        if (towerComponentB.shotOrigins != null) {
          towerComponentB.targets.add(entityA);
        } else {
          EnemyComponent enemyComponentA = Mapper.enemyMapper.get(entityA);
          if (enemyComponentA != null) {
            enemyComponentA.hp -= 1f;
          }
        }
        return;
      }
    }

    // B seems to usually be what we want, so prioritize it.
    PathFollowComponent pathFollowComponentB = Mapper.pathFollowMapper.get(entityB);
    if (pathFollowComponentB != null) {
      DynamicsComponent dynamicsB = Mapper.dynamicsMapper.get(entityB);
      dynamicsB.activeContact = true;
    } else {
      PathFollowComponent pathFollowComponentA = Mapper.pathFollowMapper.get(entityA);
      if (pathFollowComponentA != null) {
        DynamicsComponent dynamicsA = Mapper.dynamicsMapper.get(entityB);
        dynamicsA.activeContact = true;
      }
    }

    ProjectileComponent projectileComponentA = Mapper.projectileMapper.get(entityA);
    if (projectileComponentA != null) {
      EnemyComponent enemyComponentB = Mapper.enemyMapper.get(entityB);
      if (enemyComponentB != null) {
        enemyComponentB.hp -= projectileComponentA.damage;
      }
    } else {
      ProjectileComponent projectileComponentB = Mapper.projectileMapper.get(entityB);
      if (projectileComponentB != null) {
        EnemyComponent enemyComponentA = Mapper.enemyMapper.get(entityA);
        if (enemyComponentA != null) {
          enemyComponentA.hp -= projectileComponentB.damage;
        }
      }
    }
  }

  @Override
  public void endContact(Contact contact) {
    Logger.dbg("Contact ended.");

    Entity entityA = (Entity) contact.getFixtureA().getUserData();
    Entity entityB = (Entity) contact.getFixtureB().getUserData();

    TowerComponent towerComponentA = Mapper.towerMapper.get(entityA);
    if (towerComponentA != null) {
      towerComponentA.targets.removeValue(entityB, true);
      return;
    } else {
      TowerComponent towerComponentB = Mapper.towerMapper.get(entityB);
      if (towerComponentB != null) {
        towerComponentB.targets.removeValue(entityA, true);
        return;
      }
    }

    PathFollowComponent pathFollowComponentB = Mapper.pathFollowMapper.get(entityB);
    if (pathFollowComponentB != null) {
      DynamicsComponent dynamicsB = Mapper.dynamicsMapper.get(entityB);
      dynamicsB.activeContact = false;
      dynamicsB.reeling = true;
    } else {
      PathFollowComponent pathFollowComponentA = Mapper.pathFollowMapper.get(entityA);
      if (pathFollowComponentA != null) {
        DynamicsComponent dynamicsA = Mapper.dynamicsMapper.get(entityB);
        dynamicsA.activeContact = false;
        dynamicsA.reeling = true;
      }
    }

    ProjectileComponent projectileComponentA = Mapper.projectileMapper.get(entityA);
    if (projectileComponentA != null) {
      EnemyComponent enemyComponentB = Mapper.enemyMapper.get(entityB);
      if (enemyComponentB != null) {
        Mapper.dynamicsMapper.get(entityA).isDead = true;
      }
    } else {
      ProjectileComponent projectileComponentB = Mapper.projectileMapper.get(entityB);
      if (projectileComponentB != null) {
        EnemyComponent enemyComponentA = Mapper.enemyMapper.get(entityA);
        if (enemyComponentA != null) {
          // TODO: See above.
          Mapper.dynamicsMapper.get(entityB).isDead = true;
        }
      }
    }
  }

  // TODO: Do these need to be filled in with anything?
  @Override
  public void preSolve(Contact contact, Manifold oldManifold) {}

  @Override
  public void postSolve(Contact contact, ContactImpulse impulse) {}
}
