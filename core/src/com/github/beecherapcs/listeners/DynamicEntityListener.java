package com.github.beecherapcs.listeners;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

import com.github.beecherapcs.Mapper;
import com.github.beecherapcs.components.BodyComponent;
import com.github.beecherapcs.components.DynamicsComponent;
import com.github.beecherapcs.components.KinematicsComponent;
import com.github.beecherapcs.utils.Logger;

// This class hooks the creation of dynamic entities, creating a corresponding Box2D object.
public class DynamicEntityListener implements EntityListener {
  private final World world;

  public DynamicEntityListener(World world) {
    this.world = world;
  }

  public void entityAdded(Entity entity) {
    Logger.log("Creating body for dynamic entity.");

    KinematicsComponent kinematics = Mapper.kinematicsMapper.get(entity);
    DynamicsComponent dynamics = Mapper.dynamicsMapper.get(entity);
    Sprite sprite = Mapper.spriteMapper.get(entity).sprite;

    if (kinematics.position != null) {
      dynamics.bodyDef.position.set(kinematics.position);
    } else {
      Logger.err("Position is unset, using origin.");
      dynamics.bodyDef.position.set(new Vector2());
    }
    if (kinematics.velocity != null) {
      dynamics.bodyDef.linearVelocity.set(kinematics.velocity);
    } else {
      Logger.err("Velocity is unset, ignoring.");
    }
    Body body = world.createBody(dynamics.bodyDef);
    body.createFixture(dynamics.fixtureDef);
    // Add the entity to the fixture so that Box2D can refer to it during a collision. Self
    // referential!
    // TODO: do we need to do this for all entities? (check for pathFollow)
    body.getFixtureList().get(0).setUserData(entity);

    if (Mapper.towerMapper.get(entity) != null) {
      BodyDef hingeBodyDef = new BodyDef();
      hingeBodyDef.type = BodyDef.BodyType.StaticBody;
      hingeBodyDef.position.set(dynamics.bodyDef.position);
      Body hingeBody = world.createBody(hingeBodyDef);

      RevoluteJointDef jointDef = new RevoluteJointDef();
      jointDef.initialize(body, hingeBody, new Vector2(hingeBodyDef.position));
      world.createJoint(jointDef);
    }

    entity.add(new BodyComponent(body));
  }

  public void entityRemoved(Entity entity) {
    // In the path system, we only removed the kinematics component, because if we removed
    // everything then this
    // method wouldn't be able to destroy the body.
    world.destroyBody(Mapper.bodyMapper.get(entity).body);
    // At this point, we *can* remove everything.
    entity.removeAll();
  }
}
