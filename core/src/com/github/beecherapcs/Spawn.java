package com.github.beecherapcs;

import com.github.beecherapcs.entitytypes.EnemyEntityType;

public class Spawn implements Comparable<Spawn> {
  // The time that the enemy will be spawned at.
  public float t;
  public EnemyEntityType enemyType;

  public Spawn(float t, EnemyEntityType enemyType) {
    this.t = t;
    this.enemyType = enemyType;
  }

  @Override
  public int compareTo(Spawn other) {
    if (this.t == other.t) return 0;
    else if (this.t > other.t) return 1;
    else return -1;
  }
}
