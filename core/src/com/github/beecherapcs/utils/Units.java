package com.github.beecherapcs.utils;

public class Units {
  // Box2D operates in meters, and it's advised to operate in the same units for graphics as you do
  // for Box2D. So we want the world units to be meters.
  //      1 World Unit = 1 Meter
  // To put things into perspective, we can use our background image to get a sense of what 1
  // meter is. Let's say that 1 meter is about the width of the path that the enemies travel on,
  // in a BTD map is a good benchmark for this. Looking at the BTD6 Monkey Meadows map, the path
  // is about 31-33 pixels wide. Therefore:
  //      1 World Unit = Width of BTD6 path = 32px
  // These world units are used by the viewport (and by extension the camera), the entity
  // coordinate system, and Box2D.
  // TODO: factor out as constant?
  public static final int pixelsPerWorldUnit = 32;

  public static float pixelsToWorldUnits(float pixels) {
    return pixels / pixelsPerWorldUnit;
  }

  public static float worldUnitsToPixels(float worldUnits) {
    return worldUnits * pixelsPerWorldUnit;
  }
}
