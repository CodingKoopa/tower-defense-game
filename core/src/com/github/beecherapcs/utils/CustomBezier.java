package com.github.beecherapcs.utils;

import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Vector2;

public class CustomBezier extends Bezier<Vector2> {
  private final int numSplinePoints = 10000;
  private final Vector2[] splinePoints = new Vector2[numSplinePoints];

  @SafeVarargs
  public CustomBezier(final Vector2... points) {
    set(points);
    for (int i = 0; i < numSplinePoints; ++i) {
      splinePoints[i] = new Vector2();
      valueAt(splinePoints[i], ((float) i / (numSplinePoints - 1)));
    }
  }

  @Override
  public float locate(Vector2 v) {
    int candidate = numSplinePoints - 1;
    float minDistance = v.dst(splinePoints[candidate]);
    for (int i = numSplinePoints - 2; i >= 0; --i) {
      Vector2 splinePoint = splinePoints[i];
      float distance = v.dst(splinePoint);
      if (distance < minDistance) {
        candidate = i;
        minDistance = distance;
      }
    }
    return (float) candidate / (numSplinePoints - 1);
  }
}
