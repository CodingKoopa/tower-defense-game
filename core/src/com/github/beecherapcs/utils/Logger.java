package com.github.beecherapcs.utils;

import com.badlogic.gdx.Gdx;

public class Logger {
  public static final void log(String message) {
    Gdx.app.log(tag(), message);
  }

  public static final void err(String message) {
    Gdx.app.error(tag(), message);
  }

  public static final void dbg(String message) {
    Gdx.app.debug(tag(), message);
  }

  private static String tag() {
    String className = Thread.currentThread().getStackTrace()[3].getClassName();
    return className.substring(className.lastIndexOf(".") + 1)
        + "."
        + Thread.currentThread().getStackTrace()[3].getMethodName();
  }
}
