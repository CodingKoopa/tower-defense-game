package com.github.beecherapcs.utils;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

// TODO: Enforce a minimum window size.
public class LeftFitViewport extends ScalingViewport {
  private final float accommodateWidth;

  public LeftFitViewport(float worldWidth, float worldHeight, float accommodateWidth) {
    super(Scaling.fit, worldWidth, worldHeight);
    this.accommodateWidth = accommodateWidth;
  }

  public LeftFitViewport(
      float worldWidth, float worldHeight, Camera camera, float accommodateWidth) {
    super(Scaling.fit, worldWidth, worldHeight, camera);
    this.accommodateWidth = accommodateWidth;
  }

  public void update(int screenWidth, int screenHeight, boolean centerCamera) {
    Vector2 scaled =
        Scaling.fit.apply(
            getWorldWidth(), getWorldHeight(), screenWidth - accommodateWidth, screenHeight);
    int viewportWidth = Math.round(scaled.x);
    int viewportHeight = Math.round(scaled.y);

    // Shift to the left.
    setScreenBounds(0, (screenHeight - viewportHeight) / 2, viewportWidth, viewportHeight);

    apply(centerCamera);
  }
}
