package com.github.beecherapcs.utils;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.FloatArray;

// Utility class for creating and scaling enemy HP bars.
public class HpBar {
  // Vertices here are treated as pixels.

  // Left Side:
  //   O
  //  O
  // OOO
  private static final float[] leftSideVerts = {
    2f, 0f,
    0f, 0f,
    2f, 2f
  };
  // Right Side:
  // OOO
  //  O
  // O
  private static final float[] rightSideVerts = {
    0f, 2f,
    2f, 2f,
    0f, 0f
  };

  static final float unitsPerHp = 3;
  // Convert from pixels to world units, and scale up to satisfaction.
  static final float scale = 4.0f * Units.pixelsToWorldUnits(1.0f);

  public static Polygon makeHpBar(float amount) {
    float[] verts = new float[leftSideVerts.length + rightSideVerts.length];
    // Add the vertices for the left side, in order.
    for (int i = 0; i < leftSideVerts.length; ++i) verts[i] = leftSideVerts[i];
    // Calculate how much further away the right side has to be. The last vertex in the left side
    // is used as the tail of the left.
    float startRight = leftSideVerts[leftSideVerts.length - 2] + amount * unitsPerHp;
    // Add the vertices for the right side, in order.
    // TODO: check that size is even
    for (int i = 0; i < rightSideVerts.length; ++i) {
      verts[leftSideVerts.length + i] = startRight + rightSideVerts[i];
      ++i;
      verts[leftSideVerts.length + i] = rightSideVerts[i];
    }

    Polygon polygon = new Polygon(verts);
    // Setup the polygon to scale up to world units when transformed.
    polygon.setScale(scale, scale);
    // Place the center of the polygon at the bottom center of the vertices.
    polygon.setOrigin(-polygon.getBoundingRectangle().getWidth() / 2, 0);
    return polygon;
  }

  public static Polygon cutPolygon(Polygon polygon, float percent) {
    if (percent >= 1.0f) return polygon;

    // The bounding rectangle operates in scaled world units, which is no good because we need to
    // initialized polygon with untransformed vertices.
    float maxX = polygon.getBoundingRectangle().getWidth() / scale * percent;

    float[] verts = polygon.getVertices();
    FloatArray newVerts = new FloatArray(verts.length);

    boolean lastVertWasOob = false;
    float x = 0;
    float y = 0;
    Float lastX = null;
    Float lastY = null;

    for (int i = 0; i < verts.length; ++i) {
      x = verts[i];
      ++i;
      y = verts[i];

      // If this vert is out of bounds, we can't draw it.
      if (x > maxX) {
        // If we just went out of bounds, we want to add a vertex between the last good point, and
        // this out of bounds point.
        if (!lastVertWasOob && lastX != null) {
          // The x-value is as far right as we can go.
          newVerts.add(maxX);
          // Use linear interpolation to get the y-value.
          float slope = (y - lastY) / (x - lastX);
          newVerts.add(lastY + slope * (maxX - lastX));
        }
        lastVertWasOob = true;
      } else {
        // If we just came in bounds, we want to add a vertex between this good point, and the
        // last out of bounds point.
        if (lastVertWasOob) {
          newVerts.add(maxX);
          float slope = (y - lastY) / (x - lastX);
          newVerts.add(y + slope * (maxX - x));
        }

        newVerts.add(x);
        newVerts.add(y);
        lastVertWasOob = false;
      }

      lastX = x;
      lastY = y;
    }

    // If we ended while OOB, LERP one more point.
    if (lastVertWasOob) {
      lastX = verts[0];
      lastY = verts[1];
      newVerts.add(maxX);
      float slope = (y - lastY) / (x - lastX);
      newVerts.add(lastY + slope * (maxX - x));
    }

    newVerts.shrink();
    Polygon newPolygon = new Polygon(newVerts.items);
    // Copy the attributes from the original polygon.
    newPolygon.setOrigin(polygon.getOriginX(), polygon.getOriginY());
    newPolygon.setScale(polygon.getScaleX(), polygon.getScaleY());
    newPolygon.setPosition(polygon.getX(), polygon.getY());

    return newPolygon;
  }
}
