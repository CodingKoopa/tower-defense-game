package com.github.beecherapcs;

public enum Box2dCategory {
  Default(0b00001),
  Tower(0b00010),
  Bullet(0b00100),
  Arrow(0b01000),
  Enemy(0b10000);

  public final short value;

  Box2dCategory(int value) {
    // Do the cast here because it's less typing.
    this.value = (short) value;
  }
}
