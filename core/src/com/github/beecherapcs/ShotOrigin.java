package com.github.beecherapcs;

import com.badlogic.gdx.math.Vector2;

public class ShotOrigin {
  public final Vector2 relPos;
  public final Vector2 vel;

  public ShotOrigin(Vector2 relPos, Vector2 vel) {
    this.relPos = relPos;
    this.vel = vel;
  }
}
