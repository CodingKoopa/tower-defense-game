package com.github.beecherapcs;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.Viewport;

import com.github.beecherapcs.components.*;
import com.github.beecherapcs.entitytypes.ShooterTowerEntityType;
import com.github.beecherapcs.entitytypes.SpinnerTowerEntityType;
import com.github.beecherapcs.entitytypes.TowerEntityType;
import com.github.beecherapcs.screens.GameScreen;

public class TowerSpawner {
  private final Engine engine;
  private final Viewport worldViewport;
  private Table table;
  private final Skin skin;
  private final GameScreen gameScreen;

  public TowerSpawner(
      Engine engine, final GameScreen gameScreen, Viewport worldViewport, Skin skin) {
    this.engine = engine;
    this.gameScreen = gameScreen;
    this.worldViewport = worldViewport;
    this.skin = skin;
  }

  public void initTowers(Table towerTable) {
    table = towerTable;

    addTower(new ShooterTowerEntityType());
    addTower(new SpinnerTowerEntityType());
  }

  // TODO: Refactor this so that it can be called from GameScreen rather than when the ctor runs.
  private void addTower(final TowerEntityType towerEntityType) {
    Table towerCell = new Table(skin);
    towerCell.setUserObject(towerEntityType.getCost());
    Texture t = towerEntityType.getSpriteComponent().texture;
    Drawable d = new TextureRegionDrawable(new TextureRegion(t));
    Image i = new Image(d, Scaling.fit);
    i.setUserObject(towerEntityType);
    i.setTouchable(Touchable.enabled);
    i.addListener(
        new InputListener() {
          // Temporary entity to reuse while the tower is being dragged.
          private final Entity tmpEntity = new Entity();
          // Position vector for the temporary entity to use.
          private final Vector2 tmpPosition = new Vector2();

          @Override
          public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if (towerEntityType.getCost() <= gameScreen.money
                && gameScreen.isPlaying
                && !gameScreen.isPaused) {
              touchUpdate(event);
              tmpEntity.add(new KinematicsComponent(tmpPosition, null));
              tmpEntity.add((towerEntityType.getSpriteComponent()));
              engine.addEntity(tmpEntity);

              return true;
            } else {
              return false;
            }
          }

          @Override
          public void touchDragged(InputEvent event, float x, float y, int pointer) {
            touchUpdate(event);
          }

          private void touchUpdate(InputEvent event) {
            // Set the vector to the screen coordinates of the touch.
            tmpPosition.set(event.getStageX(), event.getStageY());
            // Touch coordinates are y-down - convert them to y-up..
            tmpPosition.y = event.getStage().getHeight() - tmpPosition.y;
            // Transform the screen coordinates to the world coordinates for the target viewport.
            worldViewport.unproject(tmpPosition);
            // Update the shared position.
            tmpPosition.set(tmpPosition.x, tmpPosition.y);
          }

          @Override
          public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            engine.removeEntity(tmpEntity);
            tmpEntity.removeAll();
            spawnTower(towerEntityType, tmpPosition);
            gameScreen.money -= towerEntityType.getCost();
          }
        });
    // TODO: More precise/better value for this.
    towerCell.add(i).size(100);
    towerCell.row();
    towerCell.add("$" + towerEntityType.getCost()).expandX();
    table.add(towerCell).expandX();
  }

  public void spawnTower(TowerEntityType towerEntityType, Vector2 position) {
    Entity towerEntity = new Entity();
    towerEntity.add(new KinematicsComponent(position.cpy()));
    towerEntity.add(
        new DynamicsComponent(towerEntityType.getBodyDef(), towerEntityType.getFixtureDef()));
    towerEntity.add(
        new TowerComponent(
            towerEntityType.getCost(),
            towerEntityType.getShotOrigins(),
            towerEntityType.getShootingRate()));
    towerEntity.add(towerEntityType.getSpriteComponent());

    engine.addEntity(towerEntity);
  }
}
