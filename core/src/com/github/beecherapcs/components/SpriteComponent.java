package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

// This component contains the information required to draw an entity,
public class SpriteComponent implements Component {
  public final Sprite sprite;
  public final Texture texture;

  public SpriteComponent(String path, float width, float height) {
    texture = new Texture(Gdx.files.internal(path));
    sprite = new Sprite(texture);
    sprite.setSize(width, height);
    // This is important - this makes sure that sprites are drawn in the correct location, and is
    // used by the shooter system to determine where to spawn shots.
    sprite.setOriginCenter();
  }
}
