package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;

// This component contains information that pertains to a projectile entity that is spawned by a
// tower entity.
public class ProjectileComponent implements Component {
  public float damage;

  public ProjectileComponent(float damage) {
    this.damage = damage;
  }
}
