package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;

// This component contains a Box2D body object corresponding to an entity.
public class BodyComponent implements Component {
  public final Body body;

  public BodyComponent(Body body) {
    this.body = body;
  }
}
