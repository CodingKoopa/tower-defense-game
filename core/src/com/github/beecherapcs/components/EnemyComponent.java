package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Polygon;

import com.github.beecherapcs.utils.HpBar;

// This component contains information pertaining to enemies, such as their HP (Health Points).
public class EnemyComponent implements Component {
  public final float maxHp;
  public float hp;
  // Cached polygon for the full HP bar.
  public final Polygon fullHpBar;
  // Amount of damage this enemy deals when it gets by.
  public final int damage;

  public EnemyComponent(float maxHp, int damage) {
    this.maxHp = maxHp;
    hp = this.maxHp;
    this.damage = damage;
    fullHpBar = HpBar.makeHpBar(maxHp);
  }
}
