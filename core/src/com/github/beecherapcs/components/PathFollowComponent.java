package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

// This component contains information that pertains to entities that follow paths, such as the
// current path segment they are traversing.
public class PathFollowComponent implements Component {
  // The speed that this entity travels along the path at.
  public final float speed;

  public int segmentIdx = 0;
  public float t = 0;
  public float tEnd = 0;
  // The score is calculated from both the segment and the t value.
  public float score = 0;
  // The position that Box2D wishes to apply to the entity.
  public Vector2 boxDesiredPosition = new Vector2();
  // The velocity that Box2D wishes to apply to the entity.
  public Vector2 boxDesiredVelocity = new Vector2();
  // The velocity that the path wishes to apply to the entity.
  public Vector2 pathDesiredVelocity = new Vector2();

  public PathFollowComponent(float speed) {
    this.speed = speed;
  }
}
