package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

// This component describes the dynamics of an entity, including its size and mass.
public class DynamicsComponent implements Component {
  public BodyDef bodyDef;
  public FixtureDef fixtureDef;
  // Whether this body is actively in contact with another object,
  public boolean activeContact = false;
  public boolean reeling = false;
  public boolean isDead = false;

  public DynamicsComponent(BodyDef bodyDef, FixtureDef fixtureDef) {
    this.bodyDef = bodyDef;
    this.fixtureDef = fixtureDef;
  }
}
