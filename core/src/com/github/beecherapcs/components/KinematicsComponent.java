package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

// This component describes the kinematics of an entity, including its position and velocity
// vectors.
public class KinematicsComponent implements Component {
  public final Vector2 position;
  public final Vector2 velocity;

  public KinematicsComponent() {
    this(new Vector2(), new Vector2());
  }

  public KinematicsComponent(Vector2 position) {
    this(position, new Vector2());
  }

  public KinematicsComponent(Vector2 position, Vector2 velocity) {
    this.position = position;
    this.velocity = velocity;
  }
}
