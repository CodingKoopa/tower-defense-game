package com.github.beecherapcs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.Array;

import com.github.beecherapcs.ShotOrigin;

// This component contains information that pertains to towers.
public class TowerComponent implements Component {
  public final int cost;

  public final ShotOrigin[] shotOrigins;

  public final float rate;
  public float accumulator;

  public final Array<Entity> targets = new Array<>();

  public TowerComponent(int cost, ShotOrigin[] shotOrigins, float rate) {
    this.cost = cost;
    this.shotOrigins = shotOrigins;
    this.rate = rate;
    // Shoot immediately.
    accumulator = rate;
  }
}
