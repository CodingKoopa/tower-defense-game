package com.github.beecherapcs;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.github.beecherapcs.screens.MainMenuScreen;
import com.github.beecherapcs.utils.Logger;

public class TowerDefenseGame extends Game {
  public SpriteBatch batch;
  public BitmapFont font;
  public ShapeRenderer sr;
  public Stage st;

  @Override
  public void create() {
    // TODO: Make this configurable
    Gdx.app.setLogLevel(Application.LOG_DEBUG);

    Logger.log("Initializing game.");

    // Initialize the sprite batch.
    batch = new SpriteBatch();
    // Initialize the font.
    font = new BitmapFont();
    // Set the default screen to the main menu screen.
    setScreen(new MainMenuScreen(this));
  }

  @Override
  public void dispose() {
    // Dispose of the objects that have been created.
    batch.dispose();
    font.dispose();
  }
}
