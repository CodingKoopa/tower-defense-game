package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.CircleShape;

import com.github.beecherapcs.components.SpriteComponent;

public class DummyEnemyEntityType extends EnemyEntityType {
  private final float width = 1f;
  private final float height = 1f;

  private final SpriteComponent spriteComponent =
      new SpriteComponent("textures/BallEnemy.png", width, height);

  public DummyEnemyEntityType() {
    CircleShape circle = new CircleShape();
    circle.setRadius(width / 2);
    fixtureDef.shape = circle;
    fixtureDef.density = 1;
  }

  @Override
  public SpriteComponent getSpriteComponent() {
    return spriteComponent;
  }

  @Override
  public float getMaxHp() {
    return 2f;
  }

  @Override
  public int getDamage() {
    return 5;
  }

  @Override
  public float getSpeed() {
    return 1.5f;
  }
}
