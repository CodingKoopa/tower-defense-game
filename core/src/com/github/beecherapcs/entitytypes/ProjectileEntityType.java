package com.github.beecherapcs.entitytypes;

public abstract class ProjectileEntityType extends BaseEntityType {
  public abstract float getDamage();
}
