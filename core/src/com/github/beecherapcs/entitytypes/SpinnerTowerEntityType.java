package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import com.github.beecherapcs.ShotOrigin;
import com.github.beecherapcs.components.SpriteComponent;
import com.github.beecherapcs.utils.Units;

public class SpinnerTowerEntityType extends TowerEntityType {
  private final float width = 1f;
  private final float height = 2f;

  Vector2 center = new Vector2(0.5f, 0.5f);

  // Woo! Hardcoded vertices!
  float[] collision = new float[] {13, 40, 11, 38, 10, 33, 10, 29, 15, 0, 16, 0};

  public SpinnerTowerEntityType() {
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    //    bodyDef.bullet = true;
    bodyDef.awake = true;

    for (int i = 0; i < collision.length; ++i) {
      // If this is a y value.
      if (i % 2 != 0) {
        // Convert to y-up.
        collision[i] = Units.worldUnitsToPixels(height) - collision[i];
      }
      collision[i] /= Units.pixelsPerWorldUnit;
      // TODO: I hate this and I don't really know why this works and I want to go home.
      if (i % 2 != 0) {
        collision[i] -= center.y;
      } else {
        collision[i] -= center.x;
      }
    }

    PolygonShape shape = new PolygonShape();
    shape.set(collision);
    fixtureDef.shape = shape;
    fixtureDef.isSensor = false;
    fixtureDef.density = 10;
  }

  @Override
  public SpriteComponent getSpriteComponent() {
    // TODO: cache texture
    SpriteComponent sc = new SpriteComponent("textures/Spinner.png", width, height);
    sc.sprite.setOrigin(center.x, center.y);
    return sc;
  }

  @Override
  public int getCost() {
    return 300;
  }

  @Override
  public ShotOrigin[] getShotOrigins() {
    return null;
  }

  @Override
  public float getShootingRate() {
    return 0.2f;
  }
}
