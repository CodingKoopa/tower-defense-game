package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;

import com.github.beecherapcs.Box2dCategory;
import com.github.beecherapcs.ShotOrigin;

public abstract class TowerEntityType extends BaseEntityType {
  protected CircleShape circle = new CircleShape();

  public TowerEntityType() {
    // Towers are dynamic bodies because they can interact with other objects.
    bodyDef.type = BodyDef.BodyType.DynamicBody;

    CircleShape shape = new CircleShape();
    shape.setRadius(2f);
    fixtureDef.shape = shape;
    fixtureDef.filter.categoryBits = Box2dCategory.Tower.value;
    // Towers can't collide with other towers or bullets.
    fixtureDef.filter.maskBits &= ~(Box2dCategory.Tower.value & Box2dCategory.Bullet.value);
    fixtureDef.isSensor = true;
  }

  public abstract int getCost();

  public abstract ShotOrigin[] getShotOrigins();

  public abstract float getShootingRate();
}
