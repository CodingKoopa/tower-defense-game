package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import com.github.beecherapcs.Box2dCategory;
import com.github.beecherapcs.components.SpriteComponent;

public class ArrowEntityType extends ProjectileEntityType {
  private final float radius = 0.25f;

  // TODO: unique art
  private final SpriteComponent spriteComponent =
      new SpriteComponent("textures/Bullet.png", radius, radius);

  public ArrowEntityType() {
    // Bullets are kinematic bodies because they just move at their velocity.
    bodyDef.type = BodyDef.BodyType.KinematicBody;
    // Prevent tunnelling through other dynamic objects.
    bodyDef.bullet = true;

    PolygonShape shape = new PolygonShape();
    shape.setAsBox(radius / 2, radius / 2);
    fixtureDef.shape = shape;
    fixtureDef.filter.categoryBits = Box2dCategory.Arrow.value;
    // Treat this as a sensor; we'll handle the collisions ourselves.
    fixtureDef.isSensor = true;
  }

  @Override
  public SpriteComponent getSpriteComponent() {
    return spriteComponent;
  }

  @Override
  public float getDamage() {
    return 1f;
  }
}
