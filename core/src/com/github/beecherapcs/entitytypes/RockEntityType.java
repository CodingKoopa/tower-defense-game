package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import com.github.beecherapcs.Box2dCategory;
import com.github.beecherapcs.components.SpriteComponent;

public class RockEntityType extends ProjectileEntityType {
  private final float radius = 0.25f;

  private final SpriteComponent spriteComponent =
      new SpriteComponent("textures/Bullet.png", radius, radius);

  public RockEntityType() {
    // Bullets are dynamics bodies because they interact with the enemies.
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    // Prevent tunnelling through other dynamic objects.
    bodyDef.bullet = true;

    PolygonShape shape = new PolygonShape();
    shape.setAsBox(radius / 2, radius / 2);
    fixtureDef.shape = shape;
    fixtureDef.filter.categoryBits = Box2dCategory.Bullet.value;
  }

  @Override
  public SpriteComponent getSpriteComponent() {
    return spriteComponent;
  }

  @Override
  public float getDamage() {
    return 0.5f;
  }
}
