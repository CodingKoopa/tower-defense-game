package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import com.github.beecherapcs.Box2dCategory;
import com.github.beecherapcs.components.SpriteComponent;

// Base class for all entity types.
public abstract class BaseEntityType {
  protected BodyDef bodyDef = new BodyDef();
  // TODO: Free the shape used here... somehow. Perhaps hook engine de-init and iterate.
  protected FixtureDef fixtureDef = new FixtureDef();

  public BodyDef getBodyDef() {
    return bodyDef;
  }

  public FixtureDef getFixtureDef() {
    if (fixtureDef.shape == null)
      throw new RuntimeException(
          "Fixture shape is null. Please set this when subclassing BaseEntityType!");
    if (fixtureDef.filter.categoryBits == Box2dCategory.Default.value)
      throw new RuntimeException(
          "Fixture category is the default. Please set this when subclassing BaseEntityType!");
    return fixtureDef;
  }

  // Gets the sprite component for this entity type. The sprite contains (among other things) the
  // position and rotation data for the sprite. The position we constantly update, in the sprite
  // system. The rotation, however, is set only sometimes, on a per-entity basis, by the shooter
  // system. Therefore, if the entity may be rotating, this method *must* return unique instances.
  // Otherwise, you can just initialize one SpriteComponent and share it.
  // TODO: For unique instances of this, implement pooling.
  public abstract SpriteComponent getSpriteComponent();
}
