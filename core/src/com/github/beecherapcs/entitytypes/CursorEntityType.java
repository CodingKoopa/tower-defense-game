package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import com.github.beecherapcs.Box2dCategory;
import com.github.beecherapcs.components.SpriteComponent;

public class CursorEntityType extends BaseEntityType {
  private final float width = 1f;
  private final float height = 1f;

  private final SpriteComponent spriteComponent =
      new SpriteComponent("textures/Bullet.png", width, height);

  public CursorEntityType() {
    // Enemies are dynamic bodies because they need to respond to forces.
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    bodyDef.linearDamping = 1f;
    bodyDef.fixedRotation = true;

    fixtureDef = new FixtureDef();
    PolygonShape shape = new PolygonShape();
    shape.setAsBox(0.5f, 0.5f);
    fixtureDef.shape = shape;
    fixtureDef.filter.categoryBits = Box2dCategory.Tower.value;
    fixtureDef.filter.maskBits = 0;
    fixtureDef.density = 1f;
  }

  @Override
  public SpriteComponent getSpriteComponent() {
    return spriteComponent;
  }
}
