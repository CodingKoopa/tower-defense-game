package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.physics.box2d.BodyDef;

import com.github.beecherapcs.Box2dCategory;

// Base class for all enemy types.
public abstract class EnemyEntityType extends BaseEntityType {
  // This is the polygon for the full HP bar outline for this enemy. This may seem a little strange,
  // as this pertains to the graphics being displayed, which is otherwise found in components. The
  // reason why this is here is that it allows us to calculate the polygon once, and reuse it for
  // all enemies of the same time - after all, this is a function of max HP.
  //  private final Polygon hpBar = unscaledHpBar;

  public EnemyEntityType() {
    // Enemies are dynamic bodies because they need to respond to forces.
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    // Reduce the linear velocity over time, to oppose motion while reeling.
    bodyDef.linearDamping = 1f;
    // All enemies have a fixed rotation.
    bodyDef.fixedRotation = true;

    fixtureDef.filter.categoryBits = Box2dCategory.Enemy.value;
    // Don't collide with other enemies.
    fixtureDef.filter.maskBits &= ~Box2dCategory.Enemy.value;
  }

  public abstract float getMaxHp();

  public abstract int getDamage();

  public abstract float getSpeed();
}
