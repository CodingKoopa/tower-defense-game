package com.github.beecherapcs.entitytypes;

import com.badlogic.gdx.math.Vector2;

import com.github.beecherapcs.ShotOrigin;
import com.github.beecherapcs.components.SpriteComponent;

public class ShooterTowerEntityType extends TowerEntityType {
  private final float width = 1f;
  private final float height = 1f;

  //  private final SpriteComponent spriteComponent =
  //      new SpriteComponent("textures/CrossbowTower.png", width, height);

  private final ShotOrigin[] shotOrigins = {
    new ShotOrigin(new Vector2(0f, 0f), new Vector2(0, 10f))
  };

  @Override
  public SpriteComponent getSpriteComponent() {
    // TODO: cache texture
    return new SpriteComponent("textures/CrossbowTower.png", width, height);
  }

  @Override
  public int getCost() {
    return 100;
  }

  @Override
  public ShotOrigin[] getShotOrigins() {
    return shotOrigins;
  }

  @Override
  public float getShootingRate() {
    return 1f;
  }
}
